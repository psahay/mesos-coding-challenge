#include "scheduler.h"
#include <vector>

int main () {
	Scheduler MyScheduler;
	vector<vector<int>> resources = {{2,3}, {7,1}, {1,23}, {8,5}, {2,8}};
	vector<vector<int>> tasks = {{3,4}, {1,4}, {4,7}, {1,3}, {5,4}};

	for (int i = 0; i < tasks.size(); ++i) {
		MyScheduler.add_node(resources[i][0], resources[i][1]);
		MyScheduler.insert_task(tasks[i][0], tasks[i][1]);
		MyScheduler.step();
		MyScheduler.display();
		cout << endl;
	}

	return 0;
}