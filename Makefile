#
# Makefile for Mesosphere Coding Challenge
#

SRCS = scheduler.cpp main.cpp
EXEC = mesos_scheduler
CPPFLAGS = --std=c++11 -g

all:
	g++ $(CPPFLAGS) -o $(EXEC) $(SRCS)

clean:
	rm -f $(EXEC)
