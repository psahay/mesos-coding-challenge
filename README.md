# README #

Build Instructions:

	make - will build the executable "mesos_scheduler"

Run:

	- runnnig the above executable shows the task distribution on nodes for the given example.
	- Please change "resources" and "tasks" vectors in the main.cpp for other inputs.
	- MyScheduler.step() simulates time ticks - must be called after adding new tasks and nodes at that tick
	- MyScheduler.display() 
		provides all tasks and time remaining currently serviced by the compute nodes with node IDs (custom in-order of receipt by stream)
	

Implementation:

	- A "Earliest Deadline First" Scheduling algorithm was implemented as opposed to the default First Come First Serve
	- Priority is decided on the basis of the time remaining left of the tasks.
	- A higher priority task can pre-empt a lower priority task.

Classes:

Node:

	- Represents a compute node with number of total resources available
	- Contains a set of tasks currently running on this node.

Task:

	- Represents task input from main.
	- Keeps track of time steps left and notifies scheduler as soon as finished.

Scheduler:

	- Manages all nodes and tasks in the system.
	- All tasks are inserted in an ordered manner in the task list.
	- Implements an Earliest Deadline First Algorithm in "service_list_EDF".
	
	Public Scheduler methods:
	
		- insert_task(resources required, time required)
		- add_node(compute node id, available resources) - takes care of duplicate entries from main
		- step()
			- Step function to simulate time. Must be called after insert_task(), add_node() at every tick.
			- takes care of ticks for each running task in the system
		
		
	
	
	
	
	
	