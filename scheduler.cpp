#include "scheduler.h"

Node :: Node(int id, int avail) {
	id_ = id;
	avail_ = avail;
	total_ = avail;
	inuse_ = 0;
}

void Node :: update_res(int avail) {
	avail_ += avail;
	total_ += avail;
}

int Node :: get_id() {
	return (id_);
}

int Node :: get_avail() {
	return (avail_);
}

void Node :: assign_task (int taskid, Task* task) {
	tasks_[taskid] = task;
	inuse_ += task->res_reqd();
	avail_ -= task->res_reqd();
}

void Node :: remove_task (int taskid) {
	inuse_ -= tasks_[taskid]->res_reqd();
	avail_ += tasks_[taskid]->res_reqd();
	tasks_[taskid]->reset_running();
	tasks_.erase(taskid);
}

/**
 * Implementation of Task class methods
 */

Task :: Task(int id, int res_req, int tleft) {
	id_ = id;
	res_req_ = res_req;
	tleft_ = tleft;
	isrunning_ = false;
}

int Task :: res_reqd() {
	return (res_req_);
}

void Task :: service_task(int node_id) {
	node_id_ = node_id;
	isrunning_ = true;
}

void Task :: reset_running() {
	isrunning_ = false;
}

int Task :: get_taskid() {
	return id_;
}

int Task :: get_nodeid() {
	return node_id_;
}

bool Task :: is_running() {
	return isrunning_;
}

int Task :: get_tleft() {
	return (tleft_);
}

int Task :: step() {
	tleft_--;
	if (tleft_ == 0) {
		return node_id_;
	}
	return (-1);
}

/**
 * Implementation of Scheduler class methods
 */

void Scheduler :: insert_task(int res_req, int task_time) {
	for (auto itr = tasklist.begin(); itr != tasklist.end(); ++itr) {
		if (task_time < (*itr)->get_tleft()) {
			tasklist.insert(itr, new Task(task_count_++, res_req, task_time));
			return;
		}
	}
	tasklist.emplace_back(new Task(task_count_++, res_req, task_time));
}

void Scheduler :: add_node(int nodeid, int res_avail) {
	if (nodemap.find(nodeid) == nodemap.end())
		nodemap[nodeid] = new Node(nodeid, res_avail);
	else 
		nodemap[nodeid]->update_res(res_avail);
}

void Scheduler :: step() {
	/* Each Running task step */
	for (auto taskitr  = tasklist.begin(); taskitr != tasklist.end(); ++taskitr) {
		if ((*taskitr)->is_running()) {
			int retval = (*taskitr)->step();
			if (retval != -1) {
				/* Task Completed */
				nodemap[retval]->remove_task((*taskitr)->get_taskid());
				delete (*taskitr);
				tasklist.erase(taskitr);
			}
		}
	}
	service_list_EDF();
}


/**
 * Implements Earliest Deadline First scheduling
 */
void Scheduler :: service_list_EDF() {

	for (auto itr = tasklist.begin(); itr != tasklist.end(); ++itr) {
		Task* task = (*itr);
		bool done = false;
		if (!task->is_running()) {
			for (auto item : nodemap) {
				Node* node = item.second;
				if (node->get_avail() >= task->res_reqd()) {
					task->service_task(node->get_id());
					node->assign_task(task->get_taskid(), task);
					done = true;
					break;
				}
			}
			if (done) continue;

			/* Try to preempt a low priority task which is running */
			for (auto enditr = tasklist.rbegin(); enditr != tasklist.rend(); ++enditr) {
				if (!(*enditr)->is_running()) continue;
				if ((*enditr)->get_tleft() <= task->get_tleft()) {
					return;
				}

				int nodeid = (*enditr)->get_nodeid();
				nodemap[nodeid]->remove_task((*enditr)->get_taskid());
				if (nodemap[nodeid]->get_avail() >= task->res_reqd()) {
					task->service_task(nodeid);
					nodemap[nodeid]->assign_task(task->get_taskid(),task);
					break;
				}
			}
		}
	}
}

void Scheduler :: display() {
	for (auto itr = nodemap.begin(); itr != nodemap.end(); ++itr) {
		cout << itr->second->get_id() << ": ";
		for (auto titr = itr->second->tasks_.begin(); titr != itr->second->tasks_.end(); ++titr) {
			cout << "(" <<titr->first << "," << titr->second->get_tleft() << ")";
		}
		cout << endl;
	}
}