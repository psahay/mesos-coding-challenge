#ifndef SCHEDULER_H
#define SCHEDULER_H

#include <iostream>
#include <list>
#include <unordered_map>

using namespace std;

/**
 * Object definition of a Compute Node
 */

class Task;

class Node {
	int id_;
	/* Resource Numbers */
	int avail_;
	int inuse_;
	int total_;
public:
	unordered_map<int, Task*> tasks_;

	Node(int id, int avail);
	void update_res(int avail);
	int get_id();
	int get_avail();
	void assign_task(int taskid, Task*);
	void remove_task(int taskid);
};

class Task {
	int id_;
	int tleft_;
	int res_req_;
	int node_id_;
	bool isrunning_;
public:
	Task(int id, int res_req, int tleft);
	int res_reqd();
	void service_task(int node_id);
	void reset_running();
	int get_taskid();
	int get_nodeid();
	bool is_running();
	int get_tleft();
	int step();
};

class Scheduler {
	int node_count_;
	int task_count_;
	list<Task*> tasklist;
	unordered_map<int, Node*> nodemap;
	void service_list_EDF();
public:
	Scheduler() {
		task_count_ = 0;
	}

	void insert_task(int res_req, int task_time);
	void add_node(int nodeid, int res_avail);
	void step();
	void display();
};

#endif /* SCEHDULER_H */